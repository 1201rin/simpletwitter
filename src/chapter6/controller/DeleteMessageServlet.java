package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

@WebServlet("/daleteMessage")
public class DeleteMessageServlet extends HttpServlet{

	@Override
	protected	void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String messgeid = request.getParameter("id");
		int id = Integer.valueOf(messgeid).intValue();
		new MessageService().delete(id);
		response.sendRedirect("./");


	}

}
